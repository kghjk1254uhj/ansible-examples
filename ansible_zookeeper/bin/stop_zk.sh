#!/bin/sh

ZK_HOME="/usr/hdp/zookeeper-3.4.14"

echo "stop zk service."

for i in 1 2 3
do
    ssh bigdata$i "$ZK_HOME/bin/zkServer.sh stop"
done
