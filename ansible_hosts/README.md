<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [作用和目录结构](#%E4%BD%9C%E7%94%A8%E5%92%8C%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84)
  - [设计说明](#%E8%AE%BE%E8%AE%A1%E8%AF%B4%E6%98%8E)
  - [文件内容介绍](#%E6%96%87%E4%BB%B6%E5%86%85%E5%AE%B9%E4%BB%8B%E7%BB%8D)
  - [运行结果](#%E8%BF%90%E8%A1%8C%E7%BB%93%E6%9E%9C)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# 作用和目录结构

ansible_hosts脚本用于集群的配置完成之后，对整个集群的/etc/hosts进行修改的自动化脚本，这个整个脚本构造如下：

```bash
[root@localhost ansible_hosts]# tree ./
./
├── group_vars  ## 存放组成员变量文件的文件夹
│   └── all  ## 组成员变量文件
├── hosts  ## 需要修改的整个集群的IP地址
├── install.sh  ## 执行剧本的脚本
├── install.yml  ## 设置执行剧本的角色
└── roles
    └── master  ## 该文件夹下放置执行的各种文件夹
        ├── files
        ├── handlers
        ├── tasks  ## 存放剧本任务文件的文件夹
        │   └── main.yml  ## 剧本任务文件
        ├── templates
        └── vars  ## 存放角色变量文件的文件夹
            └── main.yml  ## 角色变量文件
8 directories, 6 files
```

## 设计说明

FQDN的完全格式为：

```bash
192.168.217.217 master.org.cn master
```

因此可以将这三部分拆分设置为执行剧本角色的变量，然后利用尾部追加模块blockinfile将相关内容追加到后面，这样如果集群增加一台，就可以在对应的变量处增加变量和剧本任务处增加任务，达到后执行ansible即可免去登陆每一台进行操作的繁琐操作。

## 文件内容介绍

主要介绍任务文件main.yml和变量文件main.yml：

tasks/main.yml内容如下：

```yml
---
# This playbook install master service.

- name: bak /etc/hosts
  copy:
    src: /etc/hosts
    dest: /etc/hosts.bak
- name: start master fqdn debug
  debug: msg="ip is {{ master_ip[0] }}, fqdn is {{ master_fqdn[0] }}, hostname is {{ master_hostname[0] }}"
- name: start {{ agent_ip[0] }} fqdn debug
  debug: msg="ip is {{ agent_ip[0] }}, fqdn is {{ agent_fqdn[0] }}, hostname is {{ agent_hostname[0] }}"
- name: start {{ agent_ip[1] }} fqdn debug
  debug: msg="ip is {{ agent_ip[1] }}, fqdn is {{ agent_fqdn[1] }}, hostname is {{ agent_hostname[1] }}"
- name: start write the fqdn to hosts
  blockinfile:
    path: /etc/hosts
    block: |
      {{ master_ip[0] }} {{ master_fqdn[0] }} {{ master_hostname[0] }}
      {{ agent_ip[0] }} {{ agent_fqdn[0] }} {{ agent_hostname[0] }}
      {{ agent_ip[1] }} {{ agent_fqdn[1] }} {{ agent_hostname[1] }}
```

vars/main.yml内容如下：

```yml
---
# This is master FQDN
master_ip:
  - 192.168.217.221
master_fqdn:
  - master.org.cn
master_hostname:
  - master
# This is agent FQDN
agent_ip:
  - 192.168.217.222
  - 192.168.217.223
agent_fqdn:
  - agent1.org.cn
  - agent2.org.cn
agent_hostname:
  - agent1
  - agent2
```

## 运行结果

以三个节点的集群为例子，运行之后，结果如下：

```bash
[root@localhost ansible_hosts]# ./install.sh
PLAY [install master packages] **************************************************************************************************************************
TASK [Gathering Facts] **********************************************************************************************************************************
ok: [192.168.217.223]
ok: [192.168.217.222]
ok: [192.168.217.221]
TASK [master : bak /etc/hosts] **************************************************************************************************************************
ok: [192.168.217.222]
ok: [192.168.217.223]
ok: [192.168.217.221]
TASK [master : start master fqdn debug] *****************************************************************************************************************
ok: [192.168.217.221] => {
    "msg": "ip is 192.168.217.221, fqdn is master.org.cn, hostname is master"
}
ok: [192.168.217.222] => {
    "msg": "ip is 192.168.217.221, fqdn is master.org.cn, hostname is master"
}
ok: [192.168.217.223] => {
    "msg": "ip is 192.168.217.221, fqdn is master.org.cn, hostname is master"
}
TASK [master : start 192.168.217.222 fqdn debug] ********************************************************************************************************
ok: [192.168.217.221] => {
    "msg": "ip is 192.168.217.222, fqdn is agent1.org.cn, hostname is agent1"
}
ok: [192.168.217.222] => {
    "msg": "ip is 192.168.217.222, fqdn is agent1.org.cn, hostname is agent1"
}
ok: [192.168.217.223] => {
    "msg": "ip is 192.168.217.222, fqdn is agent1.org.cn, hostname is agent1"
}
TASK [master : start 192.168.217.223 fqdn debug] ********************************************************************************************************
ok: [192.168.217.221] => {
    "msg": "ip is 192.168.217.223, fqdn is agent2.org.cn, hostname is agent2"
}
ok: [192.168.217.222] => {
    "msg": "ip is 192.168.217.223, fqdn is agent2.org.cn, hostname is agent2"
}
ok: [192.168.217.223] => {
    "msg": "ip is 192.168.217.223, fqdn is agent2.org.cn, hostname is agent2"
}
TASK [master : start write the fqdn to hosts] ***********************************************************************************************************
changed: [192.168.217.223]
changed: [192.168.217.222]
changed: [192.168.217.221]
PLAY RECAP **********************************************************************************************************************************************
192.168.217.221            : ok=6    changed=1    unreachable=0    failed=0
192.168.217.222            : ok=6    changed=1    unreachable=0    failed=0
192.168.217.223            : ok=6    changed=1    unreachable=0    failed=0
[root@localhost ansible_hosts]# cat /etc/hosts.bak
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
# BEGIN ANSIBLE MANAGED BLOCK
192.168.217.221 master.org.cn master
192.168.217.222 agent1.org.cn agent1
192.168.217.223 agent2.org.cn agent2
# END ANSIBLE MANAGED BLOCK
```
