<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [ansible](#ansible)
  - [ansible_offline_install](#ansible_offline_install)
  - [ansible_hosts](#ansible_hosts)
  - [ansible_ssh](#ansible_ssh)
  - [ansible_java](#ansible_java)
  - [ansible_zookeeper](#ansible_zookeeper)
  - [ansible_hadoop](#ansible_hadoop)
  - [ansible_spark](#ansible_spark)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# ansible

【备注】：完整搭建集群，请按照下面脚本的顺序执行。

Ansible的一些配置自动化配置部署，仅上传playbooks，不包含安装包。如果要安装整个集群，请按照下面的方式进行安装。除了Ambari之外。

## ansible_offline_install

ansible离线安装之前，将影响到的主机配置好网络，并确定能够ping通网络。

## ansible_hosts

修改集群的hosts，相关的文档说明见下面链接：

[文档说明](./ansible_hosts/README.md)

## ansible_ssh

ansible_ssh是配置集群各个节点互信的自动化脚本，太简单，不介绍，只需要修改hosts，然后执行一下install.sh即可！

[文档说明](./ansible_ssh/README.md)

> 执行ansible_ssh脚本之前，需要先执行ansible_hosts之后重启，才可以正常运行。

## ansible_java

ansile_java是自动安装JDK的脚本。

[文档说明](./ansible_java/README.md)

## ansible_zookeeper

ansible_zookeeper是自动安装Zookeeper的脚本。

[文档说明](./ansible_zookeeper/README.md)

## ansible_hadoop

ansible_hadoop是自动安装Hadoop HA集群的脚本，目前仅适用于三个节点。

[文档说明](./ansible_hadoop/README.md)

## ansible_spark

ansible_spark是自动安装Spark集群的脚本，目前仅适用于三个节点。

[文档说明](./ansible_spark/README.md)
