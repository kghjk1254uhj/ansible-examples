#!/bin/sh
function offline_install(){
    rpm --nodeps -ivh ./ansible_offline_install/libyaml-0.1.4-11.el7_0.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python2-cryptography-1.7.2-2.el7.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python2-jmespath-0.9.0-3.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python2-pyasn1-0.1.9-7.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-babel-0.9.6-8.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-backports-1.0-8.el7.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-backports-ssl_match_hostname-3.5.0.1-1.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-cffi-1.6.0-5.el7.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-enum34-1.0.4-1.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-httplib2-0.9.2-1.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-idna-2.4-1.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-ipaddress-1.0.16-2.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-jinja2-2.7.2-4.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-markupsafe-0.11-10.el7.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-paramiko-2.1.1-9.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-passlib-1.6.5-2.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-ply-3.4-11.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-pycparser-2.14-1.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-setuptools-0.9.8-7.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/python-six-1.9.0-2.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/PyYAML-3.10-11.el7.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/sshpass-1.06-2.el7.x86_64.rpm
    rpm --nodeps -ivh ./ansible_offline_install/ansible-2.4.2.0-2.el7.noarch.rpm
    rpm --nodeps -ivh ./ansible_offline_install/libtirpc-0.2.4-0.16.el7.i686.rpm
    rpm --nodeps -ivh ./ansible_offline_install/libtirpc-devel-0.2.4-0.16.el7.i686.rpm
    sudo cp ./ansible_offline_install/ansible.cfg /etc/ansible/
}

offline_install